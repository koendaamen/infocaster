# Infocaster
Bij deze mijn uitwerking voor de opdracht Comment form.
Ik heb het in Laravel uitgewerkt, daar jullie vraag aan mij is om ook met Laravel aan de slag te gaan.

# homestead
Als je een homestead hebt, voeg dan toe:

## hosts
192.168.10.10 infocaster.development

## Homestead.yaml
folders:
    - map:  ~/Projects
      to:   /home/vagrant/project

sites:
    - map:  infocaster.development
      to:   /home/vagrant/project/temp/infocaster/public

databases:
    - infocaster

## Git
Clone mijn uitwerking in /home/Projects of /home/vagrant/project/temp (of pas je paden aan)

git clone git@bitbucket.org:koendaamen/infocaster.git

# DIY met laravel
Begin met een lege folder in homestead en doe:

laravel new infocaster
.env
  =>  DB_HOST     = 192.168.10.10
      DB_DATABASE = infocaster
      DB_USERNAME = homestead
      DB_PASSWORD = secret
artisan make:auth
artisan migrate
artisan make:model Author -mcr
artisan make:model Post -mcr
artisan make:model Comment -mcr
  => handwerk op models en migrations
artisan make:seeder AuthorsTableSeeder
artisan make:seeder PostsTableSeeder
artisan make:seeder CommentsTableSeeder
  => handwerk op seeders
artisan make:factory AuthorFactory --model=Author
artisan make:factory PostFactory --model=Post
artisan make:factory CommentFactory --model=Comment
  => handwerk op factories
artisan make:request StoreAuthor
artisan make:request StorePost
artisan make:request StoreComment
  => handwerk op requests, controllers en views
composer dump-autoload
artisan migrate:refresh --seed

# voila!
Ik heb er iets meer dan 2.5 uur aan gewerkt, maar dat is dus inclusief models, seeders, factories en wat fatsoenlijke views
Ik heb overigens de AuthorController verwijderd; dus geen resource/crud mogelijkheden. Een nieuwe Author is overigens aan te maken met een Register user ;-)