<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'author_id', 'post_id',
    ];
    public   $rules = [
        'title' => 'required',
        'name' => 'required',
    ];

    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
