<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\Http\Requests\StoreComment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();

        return view('comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($post_id)
    {
        $author = auth()->user()->author;
        $post = Post::findOrFail($post_id);

        return view('comments.create', compact('author', 'post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        $comment = Comment::create($request->validated());

        // TODO: fire an event, like https://laravel.com/docs/5.8/events#dispatching-events
        // event(new CommentStored($comment));
        return redirect()->route('posts.show', ['id' => $comment->post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $comments = [$comment];

        return view('comments.index', compact('comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(StoreComment $request, Comment $comment)
    {
        $validated = $request->validated();

        $comment->title = $validated['title'];
        $comment->content = $validated['content'];
        $comment->save();

        // TODO: fire an event, like https://laravel.com/docs/5.8/events#dispatching-events
        // event(new CommentUpdated($comment));
        return redirect()->route('posts.show', ['id' => $comment->post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $return_id = $comment->post->id;
        $succeeded = $comment->delete();

        if ($succeeded) {
            $message = 'Comment is successfully deleted';
        } else {
            $message = 'Comment failed to be deleted';
        }

        // TODO: fire an event, like https://laravel.com/docs/5.8/events#dispatching-events
        // event(new CommentDestroyed($comment));
        return redirect()->route('posts.show', ['id' => $return_id])->with('status', $message);
    }
}
