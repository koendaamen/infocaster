<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Requests\StorePost;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = auth()->user()->author;

        return view('posts.create', compact('author'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $post = Post::create($request->validated());

        // TODO: fire an event, like https://laravel.com/docs/5.8/events#dispatching-events
        // event(new PostStored($post));
        return redirect()->route('posts.show', ['id' => $post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, Post $post)
    {
        $validated = $request->validated();

        $post->title = $validated['title'];
        $post->content = $validated['content'];
        $post->save();

        // TODO: fire an event, like https://laravel.com/docs/5.8/events#dispatching-events
        // event(new PostUpdated($post));
        return redirect()->route('posts.show', ['id' => $post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $return_id = $post->id;
        $succeeded = $post->delete();

        if ($succeeded) {
            $message = 'Post is successfully deleted';
        } else {
            $message = 'Post failed to be deleted';
        }

        // TODO: fire an event, like https://laravel.com/docs/5.8/events#dispatching-events
        // event(new PostDestroyed($post));
        return redirect()->route('posts.show', ['id' => $return_id])->with('status', $message);
    }
}
