<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('users')->insert(
            [
                'name' => 'test',
                'email' => 'test@infocaster.developement',
                'email_verified_at' => '1971-01-01 00:00:00',
                'password' => '$2y$10$jeqm2vEEsJwj3M6bpb.hMe7CYBmxHBKS7hKTB2CitpOdckg72bKfe', // test1234
                'created_at' => '1971-01-01 00:00:00',
                'updated_at' => '1971-01-01 00:00:00',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
