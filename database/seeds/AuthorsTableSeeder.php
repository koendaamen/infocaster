<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Author::class, 1)->create()->each(function ($author) {
            $user = App\User::find(1);
            $author->user()->associate($user);
            $author->name = $user->name;
            $author->save();
        });
    }
}
