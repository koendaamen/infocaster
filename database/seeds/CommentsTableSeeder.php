<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Comment::class, 35)->create()->each(function ($comment) {
            $author = App\Author::find(1);
            $comment->author()->associate($author);
            $post = App\Post::find(rand(1, 15));
            $comment->post()->associate($post);
            $comment->save();
        });
    }
}
