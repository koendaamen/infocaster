<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Post::class, 15)->create()->each(function ($post) {
            $author = App\Author::find(1);
            $post->author()->associate($author);
            $post->save();
        });
    }
}
