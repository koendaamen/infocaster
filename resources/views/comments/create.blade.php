@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{  session('status') }}
                    </div>
                @endif

                <div class="card uper">
                    <div class="card-header">
                        Add Comment
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{  $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="post" action="{{ route('comments.store') }}">

                            @csrf
                            <input type='hidden' name='author_id' value='{{ $author->id }}'>
                            <input type='hidden' name='post_id'   value='{{ $post->id }}'>

                            <div class="form-group">
                                <label for="name">Comment Title:</label>
                                <input type="text" class="form-control" name="title"/>
                            </div>
                            <div class="form-group">
                                <label for="content">Comment Content :</label>
                                <textarea class="form-control" name="content"></textarea>
                            </div>

                            <p class='small'>
                                author id:{{ $author->id }}
                                author name:{{ $author->name }}
                                author ip:{{ $author->ip }}
                            </p>

                            <button type="submit" class="btn btn-primary">Create Comment</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection