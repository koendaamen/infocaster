@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @foreach($comments as $comment)
                <div class="card uper">
                    <div class="card-header">
                        {{ $comment->title}}<br />
                        <p class='small'>
                        id::{{ $comment->id}}
                        created_at::{{ $comment->created_at   }}
                        updated_at::{{ $comment->updated_at   }}
                        author->id::{{ $comment->author->id   }}
                        author->name::{{ $comment->author->name }}
                        author->ip::{{ $comment->author->ip   }}
                        </p>
                    </div>
                    <div class="card-body">
                        {{ $comment->content }}<br/><br/>
                        <form action="{{ route('posts.show', ['id' => $comment->post->id]) }}" method="get">
                            @csrf
                            <button class="btn" type="submit">cancel</button>
                        </form>&nbsp;
                        <form action="{{ route('comments.edit', $comment->id) }}" method="get">
                            @csrf
                            <button class="btn btn-primary" type="submit">edit</button>
                        </form>&nbsp;
                        <form action="{{ route('comments.destroy', $comment->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">delete</button>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection