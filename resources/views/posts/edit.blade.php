@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card uper">
                    <div class="card-header">
                        Edit Post
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="post" action="{{ route('posts.update', $post->id) }}">

                            @csrf
                            @method('PATCH')
                            <input type='hidden' name='author_id' value='{{ $post->author->id }}'>

                            <div class="form-group">
                                <label for="name">Post Title:</label>
                                <input type="text" class="form-control" name="title" value="{{ $post->title }}"/>
                            </div>
                            <div class="form-group">
                                <label for="content">Post Content :</label>
                                <textarea class="form-control" name="content">{{ $post->content }}</textarea>
                            </div>

                            <p class='small'>
                                author id:{{ $post->author->id }}
                                author name:{{ $post->author->name }}
                                author ip:{{ $post->author->ip }}

                            <button type="submit" class="btn btn-primary">Update Post</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection