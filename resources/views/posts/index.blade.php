@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card uper">
                    <div class="card-header">
                        <h1>Posts</h1>
                        <form action="{{ route('posts.create' ) }}" method="get">
                            @csrf
                            <button class="btn btn-primary" type="submit">add post</button>
                        </form>
                    </div>
                    <div class="card-body">
                        @foreach($posts->sortBy('updated_at')->reverse() as $post)
                            <p>
                                <a href="{{ route('posts.show', $post->id) }}">{{ $post->title }}</a> ({{ $post->id }})<br/>
                                <span class='small'>{{ $post->updated_at }}</span>
                            </p>
                        @endforeach
                    </div>

                    <form action="{{ route('posts.create' ) }}" method="get">
                        @csrf
                        <button class="btn btn-primary" type="submit">add post</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection