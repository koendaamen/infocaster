@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="{{ route('posts.index' ) }}" method="get">
                    @csrf
                    <button class="btn btn-primary" type="submit">all posts</button>
                </form>&nbsp;

                <div class="card uper">
                    <div class="card-header">
                        <h1>{{ $post->title }}</h1>
                        <p class='small'>
                        id::{{ $post->id}}
                        created_at::{{ $post->created_at   }}
                        updated_at::{{ $post->updated_at   }}
                        author->id::{{ $post->author->id   }}
                        author->name::{{ $post->author->name }}
                        author->ip::{{ $post->author->ip   }}
                        </p>
                    </div>
                    <div class="card-body">
                        {{ $post->content}}<br/><br/>
                    </div>
                </div>
                <div class="card uper">
                    <div class="card-header">
                        <h3>Comments</h3>

                        <form action="{{ route('comments.create' ) }}/{{ $post->id }}" method="get">
                            @csrf
                            <button class="btn btn-primary" type="submit">add comment</button>
                        </form>
                    </div>
                    <div class="card-body">

                        @foreach($post->comments->sortBy('updated_at')->reverse() as $comment)
                        <div class="card uper">
                            <div class="card-header">
                                <a href="{{ route('comments.show', $comment->id)}}">{{ $comment->title}}</a> ::id::{{ $comment->id}}
                                <p class='small'>
                                    {{ $comment->created_at   }}
                                    {{ $comment->updated_at   }}
                                    {{ $comment->author->id   }}
                                    {{ $comment->author->name }}
                                    {{ $comment->author->ip   }}
                                </p>
                            </div>
                            <div class="card-body">
                                {{ $comment->content }}
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection