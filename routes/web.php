<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index')->name('home');

Auth::routes();

Route::get('comments/create/{post_id}', 'CommentController@create');
Route::get('comments/create', 'PostController@index');

Route::resource('authors', 'AuthorController');
Route::resource('posts', 'PostController');
Route::resource('comments', 'CommentController');
